# sedproxy 0.6

* Fast version of ClimToProxyClim called internally when parameters are time-invariant
* Bugfix to calculation of bioturbation weights when layer.width == 0
* Other performance improvements
* Basic unit tests to detect changes to output for a standard simulation

# sedproxy 0.5.0

* Release version for revised CoTP Discussion paper
* Can now use dynamic habitat weights
* Now has explicit sensor stage with conversion from temperature to Mg/Ca or Uk'37 units
* Habitat weights can be calculated using temperature response function and parameterisation from FAME 1.0 module
* Calibration uncertainty modelled via sampling parameters from fitted calibration regression model for each replicate


# sedproxy 0.4.0

* Release version for CoTP Discussion paper

# sedproxy 0.3.1

* Added a `NEWS.md` file to track changes to the package.
* ClimToProxyClim now takes a ts object for the input climate signal.
* Bioturbation weights now take the thickness of the layer from which samples were picked/extracted into account when determining the time period over which a proxy integrates the climate signal. This is controlled by the new argument `layer.width`

